package JSExample;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class JSExample {
    private WebDriver driver;
    private final String url = "https://yandex.ru";
    private final String locator  = "#text";
    @Before
    public void before(){
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
    }

    @After
    public void after(){
        if( driver!=null )
            driver.quit();
    }

    @Test
    public void test() throws InterruptedException {
        driver.get(url);
        String s =(String) ((JavascriptExecutor) driver).executeScript("return 'hello world'");
        System.out.println(s);
        Boolean b = (Boolean) ((JavascriptExecutor) driver).executeScript("return true");
        if (b){
            System.out.println("ok");
        }

        ((JavascriptExecutor) driver).executeScript(
                "document.querySelector(\"#text\").style.display = \"none\";");            //выполняемый скрипт, который мы можем копировать из консоли


        ((JavascriptExecutor) driver).executeScript("document.querySelector('span.geolink__reg').innerHTML = \"Москва\"");

        Thread.sleep(5000);
    }
}

