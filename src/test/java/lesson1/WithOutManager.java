package lesson1;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.chrome.ChromeDriver;

public class WithOutManager {
    ChromeDriver driver;
    @Before
    public void before(){
        System.setProperty("webdriver.chrome.driver","C:\\webdriver\\chromedriver_win32\\chromedriver.exe");
        driver = new ChromeDriver();
    }


    @After
    public void after(){
        if( driver!=null )
            driver.quit();
    }

    @Test
    public void test(){
        //
        driver.get("http://yandex.ru");
        Assert.assertEquals("Яндекс", driver.getTitle());
        driver.get("http://google.ru");
    }
}
