package wait;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class WaitExample {
    private WebDriver driver;
    private final String url = "https://yandex.ru";
    private final String locator  = "#text";
    @Before
    public void before(){
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
    }

    @After
    public void after(){
        if( driver!=null )
            driver.quit();
    }

    @Test
    public void threadExample() throws InterruptedException {
        driver.get(url);
        Thread.sleep(5000);
        driver.findElement(By.cssSelector(locator)).sendKeys("dsfdf" + Keys.ENTER);
    }

    @Test
    public void implicitlyExample(){
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        driver.get(url);
        driver.findElement(By.cssSelector(locator))
                .sendKeys("dsfdf" + Keys.ENTER);
    }

    @Test
    public void explicitlyExample(){
        driver.get(url);
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(locator)))
                .sendKeys("sdfds" + Keys.ENTER);
        wait.until(ExpectedConditions.titleIs(""));
    }
}
