package lesson2;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class CookieTest {
    private WebDriver driver;
    @Before
    public void before(){
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
    }

    @After
    public void after(){
        if( driver!=null )
            driver.quit();
    }

    @Test
    public void cookieTest(){
        driver.get("https://ya.ru");
        driver.manage().addCookie(new Cookie("Key1", "Value1"));
        driver.manage().addCookie(new Cookie("Key2", "Value2"));
        Cookie cookie = new Cookie("Key3", "Value3");
        driver.manage().addCookie(cookie);
        driver.manage().addCookie(new Cookie("Key4", "Value4"));

        System.out.println(driver.manage().getCookies());
        System.out.println(driver.manage().getCookieNamed("Key1"));
        Cookie cookie1 = driver.manage().getCookieNamed("Key1");
        driver.manage().deleteCookieNamed("Key2");
        driver.manage().deleteCookie(cookie);
        driver.manage().deleteAllCookies();

        Assert.assertEquals(0, driver.manage().getCookies().size());
    }

    @Test
    public void cookie2Test(){
        driver.get("https://google.com");
        driver.manage().addCookie(new Cookie("SSID", "A4D2Sn071hoLnjn56"));
        driver.manage().addCookie(new Cookie("SID", "KAg5fTul0oP1XEWVBfiKlB5UiIzOC9f6HHoX58T_sXXPnaxVQv1Ajg8v2gnlQ8o3RRlCHw."));
        driver.manage().addCookie(new Cookie("__Secure-1PAPISID", "NISEFzrrCeOsXHL5/AZBNscoE54z6MGUv0"));
        driver.get("https://google.com");
        try {
            Thread.sleep(100000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void timeoutTest(){
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(5, TimeUnit.SECONDS);
        driver.get("https://ya.ru");
    }

    @Test
    public void windowTest(){
        driver.manage().window().maximize();
        System.out.println(driver.manage().window().getSize());

        driver.manage().window().setSize(new Dimension(800, 600));
        System.out.println(driver.manage().window().getPosition());


        Point point = driver.manage().window().getPosition();

        point.x += 50; //point.x = point.x + 50;
        point.y = point.y;
        driver.manage().window().setPosition(point);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        point.y += 50;
        driver.manage().window().setPosition(point);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        point.x -=50;
        driver.manage().window().setPosition(point);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        point.y -= 50;
        driver.manage().window().setPosition(point);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
