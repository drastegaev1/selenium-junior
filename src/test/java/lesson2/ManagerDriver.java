package lesson2;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.concurrent.TimeUnit;

public class ManagerDriver {
    protected static WebDriver driver;
    private Logger logger = LogManager.getLogger(ManagerDriver.class);

    @Before
    public void setUp() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        logger.info("Драйвер поднят");

    }

    @After
    public void setDown() {
        if (driver != null) {
            driver.quit();
        }
    }

    @Test
    public void cookie(){
        driver.get("https://ya.ru/");
        //Добавить Cookie#1 с параметром Key1 и значением Value1
        driver.manage().addCookie(new Cookie("Key1", "Value1"));
        //Добавить Cookie#2 с параметром Key2 и значением Value2
        driver.manage().addCookie(new Cookie("Key2", "Value2"));
        //Добавить Cookie#3 с параметром Key3 и значением Value3 (добавлять через переменную, переменная должна быть сохранена)
        Cookie cookie = new Cookie("Key3", "Value3");
        driver.manage().addCookie(cookie);
        //Добавить Cookie#4 с параметром Key4 и значением Value4
        driver.manage().addCookie(new Cookie("Key4", "Value4"));

        //Вывести на экран все Cookies
        logger.info(driver.manage().getCookies());
        //Вывести на экран Cookie1
        Cookie Key = driver.manage().getCookieNamed("Key1");
        logger.info(Key);
        //Удалить Cookie#2 по имени куки
        driver.manage().deleteCookieNamed("Key2");
        //Удалить Cookie#3 по переменной Cookie
        driver.manage().deleteCookie(cookie);
        //Удалить все куки, убедиться что их нет
        driver.manage().deleteAllCookies();
        Assert.assertEquals(0, driver.manage().getCookies().size());
    }

    @Test
    public void waitSample(){
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(0, TimeUnit.SECONDS);
        driver.get("https://ya.ru/");
    }

    @Test
    public void windows() throws InterruptedException {
        //Запустить тест в полном окне («не киоск»), получить его размер
        driver.manage().window().maximize();
        logger.info(driver.manage().window().getSize());
        Thread.sleep(5000);
        //Запустить тест в расширении 800 на 600, получить его позицию
        driver.manage().window().setSize(new Dimension(800,600));
        logger.info(driver.manage().window().getPosition());
        Thread.sleep(1000);
        //Передвинуть браузер по квадрату (четырем точкам)
        driver.manage().window().setSize(new Dimension(800,600));
        Point point = driver.manage().window().getPosition();

        point.x = point.x + 500;
        point.y = point.y;
        driver.manage().window().setPosition(point);
        Thread.sleep(1000);

        point.x = point.x;
        point.y = point.y + 500;
        driver.manage().window().setPosition(point);
        Thread.sleep(1000);

        point.x = point.x - 500;
        point.y = point.y;
        driver.manage().window().setPosition(point);
        Thread.sleep(1000);

        point.x = point.x;
        point.y = point.y - 500;
        driver.manage().window().setPosition(point);
        Thread.sleep(1000);
    }

    @Test
    public void headless(){
        driver.quit();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("headless");
        driver = new ChromeDriver(options);
        driver.get("https://ya.ru");
        logger.info(driver.getTitle());
    }
}